Tid Formatter
=============

Simple formatter to output raw tid.

Installation
------------

Standard module installation applies. See
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.

Usage
-----

Select 'Term ID' formatter in a display setting.
